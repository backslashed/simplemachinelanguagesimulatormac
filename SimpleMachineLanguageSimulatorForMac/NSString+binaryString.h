// Copyright (C) 2012 Backslashed

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. test

#import <Foundation/Foundation.h>

@interface NSString (binaryString)

//Converts a hex coded string to an integer
- (uint8_t)parseHexStringTo8BitInteger;
- (uint16_t)parseHexStringTo16BitInteger;

//Converts an integer to a hex coded string
+ (NSString *)hexStringWith8BitInteger:(uint8_t)integerValue;
+ (NSString *)hexStringWith16BitInteger:(uint16_t)integerValue;

//Converts an integer to a binary coded string
+ (NSString *)binaryStringWithInteger:(uint8_t)integerValue;

//Converts a binary coded string to an int
- (uint8_t)parseBinaryStringToInteger;


/*
 * Format for floats: seeemmmm
 */

//Converts a binary coded string to an float
- (float)parseBinaryStringToFloat;

//Converts a float to a binary coded string
+ (NSString *)binaryStringWithFloat:(float)floatValue;

@end
