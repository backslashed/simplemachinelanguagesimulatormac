// Copyright (C) 2012 Backslashed

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "AppDelegate.h"
#import "NSString+binaryString.h"

@interface AppDelegate()

//Get values from machine
- (void)loadCPU;
- (void)loadMemory;

//Set machine values
- (void)setCPU;
- (void)setMemory;

//Process file
- (BOOL)processFile:(NSURL *)fileURL;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //Initialize simple machine and set self as delegate
    self.machine = [[SimpleMachine alloc] init];
    [self.machine setDelegate:self];
    
    //Set filename to nil
    self.fileURL = nil;
    
    //Set window delegate
    [self.window setDelegate:self];
    
    //Register window for drag and drop
    [self.window registerForDraggedTypes:[NSArray arrayWithObjects:NSFilenamesPboardType, nil]];
    
    //Set memory matrix to be first responder
    //(so textfield has focus and we cant start typing instructions right away)
    [self.window makeFirstResponder:self.memoryMatrix];
    
    //Set self as delegate for all textfields in the matrixes
    [self.cpuMatrix setDelegate:self];
    [self.memoryMatrix setDelegate:self];
}

- (NSSize)windowWillResize:(NSWindow *)sender toSize:(NSSize)frameSize
{
    return self.window.frame.size; //Dont allow the window to be resiezed
}

- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{
    NSPasteboard *pboard;
    NSDragOperation sourceDragMask;
 
    sourceDragMask = [sender draggingSourceOperationMask];
    pboard = [sender draggingPasteboard];
    
    if ( [[pboard types] containsObject:NSFilenamesPboardType] ) {
        return NSDragOperationCopy;
    }
    
    return NSDragOperationNone;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender {
    NSPasteboard *pboard;
    NSDragOperation sourceDragMask;
 
    sourceDragMask = [sender draggingSourceOperationMask];
    pboard = [sender draggingPasteboard];
    
    if ( [[pboard types] containsObject:NSFilenamesPboardType] ) {
        NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
        NSString *filename = (NSString *)[files objectAtIndex:0];
        
        return [self processFile:[NSURL URLWithString:[[NSString stringWithFormat:@"file://localhost%@", filename] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    }
    
    return NO;
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    return YES; //Terminate application when closing window
}

- (BOOL)application:(NSApplication *)sender openFile:(NSString *)filename
{
    return [self processFile:[NSURL URLWithString:[NSString stringWithFormat:@"file://localhost%@", filename]]];
}

- (BOOL)processFile:(NSURL *)fileURL
{
    //Read contents of file to string.
    NSError *error;
    NSString *stringFromFileAtURL = [[NSString alloc]
                                     initWithContentsOfURL:fileURL
                                     encoding:NSUTF8StringEncoding
                                     error:&error];
    
    if (stringFromFileAtURL == nil) {
        // an error occurred
        NSLog(@"Error reading file at %@\n%@", fileURL, [error localizedFailureReason]);
        
        return NO;
    } else {
        NSArray *subStrings = [stringFromFileAtURL componentsSeparatedByString:@":"];
        NSString *hexString;
        
        for(int i = 1; i < [subStrings count]; i++) {
            hexString = [subStrings objectAtIndex:i];
            [self.machine setMemory:[hexString parseHexStringTo8BitInteger] atIndex:i-1];
        }
        
        //Load values from the machine
        [self loadMemory];
        
        //Reset CPU
        [self.machine resetCPU];
        [self loadCPU];
        
        //Clear status field
        [self.messageLabel setStringValue:@""];
    }
        
    return YES;
}

/*
 * User pressed run button (also stop button when running)
 */
- (IBAction)run:(id)sender
{
    if([[self.runButton title] isEqualToString:@"Run"]) {
        //Transfer values in fields to the machine
        [self setCPU];
        [self setMemory];
        
        //Get a queue
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        //Spawn of machine running on separate queue (thread) so it doesn't affect UI
        dispatch_async(queue, ^(void) {
            [self.machine run]; //Start the machine
        });
    } else {
        [self.machine stop]; //Stop the machine
    }
}

/*
 * User pressed single step button
 */
- (IBAction)singleStep:(id)sender
{
    //Transfer field values to machine
    [self setCPU];
    [self setMemory];
    
    [self.machine singleStep];
}


/*
 * User pressed reset cpu button
 */
- (IBAction)resetCPU:(id)sender
{
    //reset machine
    [self.machine resetCPU];

    //load values from machine
    [self loadCPU];
}


/*
 * User pressed reset memory button
 */
- (IBAction)resetMemory:(id)sender
{
    //reset memory
    [self.machine resetMemory];
    
    //load values from machine
    [self loadMemory];
}

//Called when user presses open
- (IBAction)openExistingDocument:(id)sender
{
    // Create and configure the panel.
    NSOpenPanel* panel = [NSOpenPanel openPanel];
    [panel setCanChooseDirectories:NO];
    [panel setAllowsMultipleSelection:NO];
    [panel setMessage:@"Open existing simulator file"];
    
    //Limit which files can be openend
    NSArray *fileTypes = [NSArray arrayWithObjects:@"SIM", @"sim", nil];
    [panel setAllowedFileTypes:fileTypes];
    
    // Display the panel attached to the document's window.
    [panel beginSheetModalForWindow:self.window completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            NSArray* urls = [panel URLs];
            
            //Store filename
            _fileURL = [urls objectAtIndex:0];
            [self processFile:_fileURL];
        }
    }];
}

//Called when user presses save
- (IBAction)saveDocument:(id)sender
{
    
    // Set the default name for the file and show the panel.
    NSSavePanel*    panel = [NSSavePanel savePanel];
    
    //Set default filename to existing (if present) file
    NSString *name = @"";
    if(_fileURL != nil) {
        name = [_fileURL lastPathComponent];
    }
    
    [panel setNameFieldStringValue:name];
    [panel beginSheetModalForWindow:self.window completionHandler:^(NSInteger result){
        if (result == NSFileHandlingPanelOKButton) {
            NSURL*  theFile = [panel URL];
            
            NSString *stringToSave = @"";
            NSTextField *memoryField = nil;
            
            //Build string
            for(int i = 0; i < 256; i++) {
                
                //Append a new line for every 16th row
                if((i%16 == 0) && (i!=0)) {
                    stringToSave = [stringToSave stringByAppendingString:@"\n"];
                }
                
                stringToSave = [stringToSave stringByAppendingString:@":"]; //add : before every value
                memoryField = (NSTextField *)[self.memoryMatrix cellAtRow:i/16 column:i%16]; //fetch textfield
                stringToSave = [stringToSave stringByAppendingString:[memoryField stringValue]]; //append value in textfield
            }
 
            // Write the contents.
            NSError *error;
            if (![stringToSave writeToURL:theFile atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
                // an error occurred
                NSLog(@"Error writing file at %@\n%@", theFile, [error localizedFailureReason]);
            }
        }
    }];
}

//New document, clear all values and filename
- (IBAction)newDocument:(id)sender
{
    //Set filename to nil
    self.fileURL = nil;
    
    //Reset machine
    [self.machine resetCPU];
    [self.machine resetMemory];
    
    //Read values from machine
    [self loadCPU];
    [self loadMemory];
    
    
}

//Loads machine register values and sets the textfields
- (void)loadCPU
{
    NSString *hexString;
    
    //Set textfields
    for(int i = 0; i < 16; i++) {
        hexString = [NSString hexStringWith8BitInteger:[self.machine getRegisterAtIndex:i]];
        [(NSTextField *)[self.cpuMatrix cellAtRow:i column:0] setStringValue:hexString];
    }
    
    //Set PC
    [(NSTextField *)[self.cpuMatrix cellAtRow:16 column:0] setStringValue:[NSString hexStringWith8BitInteger:[self.machine getProgramCounter]]];
    
    //Set IR
    hexString = [NSString hexStringWith16BitInteger:[self.machine getInstructionRegister]];
    [(NSTextField *)[self.cpuMatrix cellAtRow:17 column:0] setStringValue:hexString];
}

//Reads machine memory and sets textfields accordingly
- (void)loadMemory
{
    //Set textfields
    for(int i = 0; i < 256; i++) {
        [(NSTextField *)[self.memoryMatrix cellAtRow:i/16 column:i%16] setStringValue:[NSString hexStringWith8BitInteger:[self.machine getMemoryAtIndex:i]]];
    }
}

- (void)setCPU
{
    uint16_t instructionRegister;
    uint8_t textFieldValue;
    
    //Read 16 generel purpose registers from text fields and set machine to be the same
    for(int i = 0; i < 16; i++) {
        textFieldValue = [[(NSTextField *)[self.cpuMatrix cellAtRow:i column:0] stringValue] parseHexStringTo8BitInteger];
        [self.machine setRegister:textFieldValue atIndex:i];
    }
    
    //Read program counter from text field and set machine to be the same value
    textFieldValue = [[(NSTextField *)[self.cpuMatrix cellAtRow:16 column:0] stringValue] parseHexStringTo8BitInteger];
    [self.machine setProgramCounter:textFieldValue];
    
    //Read instruction register
    instructionRegister = [[(NSTextField *)[self.cpuMatrix cellAtRow:17 column:0] stringValue] parseHexStringTo16BitInteger];
    
    //Sets instruction register
    [self.machine setInstructionRegister:instructionRegister];
    
    
}

- (void)setMemory
{
    uint8_t memoryValue;
    
    //Read value from textfields and set machine
    for(int i = 0; i < 256; i++) {
        memoryValue = [[(NSTextField *)[self.memoryMatrix cellAtRow:i/16 column:i%16] stringValue] parseHexStringTo8BitInteger];
        [self.machine setMemory:memoryValue atIndex:i];
    }
}

/*
 * NSTextFieldDelegate
 */

- (BOOL)control:(NSControl *)control isValidObject:(id)obj
{
    NSString *text = (NSString *)obj;
    
    if(control == self.cpuMatrix) { //Allow 2 and 4 character strings in cpu matrix
        if([text length] == 2 || [text length] == 4) {
            return YES;
        } else {
            return NO;
        }
    } else { //Only length 2 is valid in memory matrix
        if([text length] == 2) {
            return YES;
        } else {
            return NO;
        }
    }
}

- (BOOL)control:(NSControl *)control textShouldEndEditing:(NSText *)fieldEditor
{
    //Transform all strings to upper case
    [control setStringValue:[[fieldEditor string] uppercaseString]];
    
    return YES;
}


/*
 * Simple machine delegate methods
 */

//Program counter was updated
- (void)didUpdateProgramCounterWithValue:(uint8_t)value
{
    //Find textfield
    NSTextField *programCounterField = [self.cpuMatrix cellAtRow:16 column:0];
    
    //Set text (Hex coded string)
    [programCounterField setStringValue:[NSString hexStringWith8BitInteger:value]];
}

//Instruction register was updated
- (void)didUpdateInstructionRegisterWithValue:(uint16_t)value
{
    //Get textfield
    NSTextField *instructionRegisterField = [self.cpuMatrix cellAtRow:17 column:0];
    
    //Set text as an hex coded string
    [instructionRegisterField setStringValue:[NSString stringWithFormat:@"%@", [NSString hexStringWith16BitInteger:value]]];
}

//Memory was updated
- (void)didUpdateMemoryAtIndex:(uint8_t)index withValue:(uint8_t)value
{
    //Get textfield
    NSTextField *memoryField = [self.memoryMatrix cellAtRow:index/16 column:index%16];
    
    //Set text as an hex coded string
    [memoryField setStringValue:[NSString hexStringWith8BitInteger:value]];
}


//Register was updated
- (void)didUpdateRegisterAtIndex:(uint8_t)index withValue:(uint8_t)value
{
    //Get field
    NSTextField *registerField = [self.cpuMatrix cellAtRow:index column:0];
    
    //Set text as hex coded string
    [registerField setStringValue:[NSString hexStringWith8BitInteger:value]];
}

//The machine sent us an message!
- (void)didRecieveMessage:(NSString *)message
{
    //Show it in the message label
    [self.messageLabel setStringValue:message];
}


//Machien began running
- (void)didBeginRunning
{
    //Disable fields while running
    [self.memoryMatrix setEnabled:NO];
    [self.cpuMatrix setEnabled:NO];
    
    //Disable buttons
    [self.resetCPUButton setEnabled:NO];
    [self.resetMemoryButton setEnabled:NO];
    [self.singleStepButton setEnabled:NO];
    
    //Start progress indicator
    [self.indicator startAnimation:self];
    
    //Change button to stop
    [self.runButton setTitle:@"Stop"];
}


//Machine stopped running
- (void)didStopRunning
{
    //Enable fields when stoppped
    [self.memoryMatrix setEnabled:YES];
    [self.cpuMatrix setEnabled:YES];
    
    //Enable buttons
    [self.resetCPUButton setEnabled:YES];
    [self.resetMemoryButton setEnabled:YES];
    [self.singleStepButton setEnabled:YES];
    
    //Stop progress indicator
    [self.indicator stopAnimation:self];
    
    //Change button to run
    [self.runButton setTitle:@"Run"];
    
}

@end
