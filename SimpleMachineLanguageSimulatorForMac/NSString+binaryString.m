// Copyright (C) 2012 Backslashed

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "NSString+binaryString.h"
#import <math.h>

@interface NSString (privateMethods)

@end

@implementation NSString (binaryString)

//Converts a hex coded string to an int
- (uint8_t)parseHexStringTo8BitInteger
{
    int result = 0;
    
    sscanf([self UTF8String], "%x", &result);
    
    return (uint8_t)result;
}

- (uint16_t)parseHexStringTo16BitInteger
{
    int result = 0;
    
    sscanf([self UTF8String], "%x", &result);
    
    return (uint16_t)result;
}

//Converts integer to an hex coded string
+ (NSString *)hexStringWith8BitInteger:(uint8_t)integerValue
{
    return [NSString stringWithFormat:@"%02X", integerValue];
}

+ (NSString *)hexStringWith16BitInteger:(uint16_t)integerValue
{
    return [NSString stringWithFormat:@"%04X", integerValue];
}

//Converts an integer to a binary coded string
+ (NSString *)binaryStringWithInteger:(uint8_t)integerValue
{
    NSMutableString *binString = [[NSMutableString alloc] init];
    
    for(int i = 0; i < 8; i++) {
        //Deternime if next bit is 1 or 0.
        if(integerValue & 1) {
            //1
            [binString insertString:@"1" atIndex:0];
        } else {
            //0
            [binString insertString:@"0" atIndex:0];
        }
        
        //Right shift one step
        integerValue = integerValue >> 1;
    }
    
    //Return string
    return [NSString stringWithString:binString];
}

//Converts a binary coded string to an int
- (uint8_t)parseBinaryStringToInteger
{
    uint8_t result = 0; /*0000 0000*/
    uint8_t invert = 1; /*0000 0001*/
    
    //Go through every character (1 or 0)
    for(int i = 0; i < 8; i++) {
        //If character in binString is 1 invert last bit of result
        if([self characterAtIndex:i] == '1') {
            result = result ^ invert; //Invert the last bit (ex: 0000 0000 ^ 0000 0001 = 0000 0001)
        }
        
        //Left shift 1 step (if not on the last bit)
        if(i < 7) {
            result = result << 1;
        }
    }
    
    //Return the result
    return result;
}


/*
 * Format for floats: seeemmmm
 */

//Converts a binary coded string to an float
//TODO: Cleanup - a bit messy now. All variables should be declared at the start...
//NOTE: 2012-10-07 23:50 - Seems to be working, debugged with E7 with matched with what i calculated for hand. Now debug the other function (probably remove + 1.0 part)
- (float)parseBinaryStringToFloat
{
    //Divide string into sign, exponent and mantissa
    char sign = [self characterAtIndex:0];
    NSString *exponentString = [self substringWithRange:NSMakeRange(1, 3)];
    NSString *mantissaString = [self substringWithRange:NSMakeRange(4, 4)];
    
    //Restore the mantissa string (add 0.)
    mantissaString = [NSString stringWithFormat:@"0.%@", mantissaString];
    
    //Calculate exponent
    exponentString = [NSString stringWithFormat:@"00000%@", exponentString];
    int exponent = [exponentString parseBinaryStringToInteger] - 4;
    
    //Denormalize number (if exponent negative move . to the left, else to the right
    float value = [mantissaString floatValue];
    if(exponent > 0) {
        for(int i = 0; i < exponent; i++) {
            value = value * 10;
        }
    } else if(exponent < 0) {
        for(int i = 0; i > exponent; i--) {
            value = value / 10;
        }
    }
    
    //Calculate integral part into decimal value
    NSString *integralString = [NSString stringWithFormat:@"%i", (int)value];
    //Make sure the string is 8 'bits' long
    if([integralString length] > 8) {
        integralString = [integralString substringWithRange:NSMakeRange(0, 8)];
    } else if ([integralString length] < 8) {
        NSMutableString *tempString = [[NSMutableString alloc] init];
        for(int i = 0; i < 8 - [integralString length]; i++) {
            [tempString appendString:@"0"];
        }
        integralString = [NSString stringWithFormat:@"%@%@", tempString, integralString];
    }
    int integral = [integralString parseBinaryStringToInteger];
    
    //Calculate binary fraction into decimal value
    NSString *fractionString = [NSString stringWithFormat:@"%f", value - (int)value];
    float fraction = 0;
    for(int i = 0; i < [fractionString length]; i++) {
        if([fractionString characterAtIndex:i] == '1') {
            fraction = fraction + pow(2, -(i-1));
        }
    }
    
    //Put integral and fraction together
    NSString *resultString = [NSString stringWithFormat:@"%f", integral + fraction];
    
    //Change sign if sign 'bit' is 1
    if(sign == '1') {
        resultString = [NSString stringWithFormat:@"-%@", resultString];
    }
    
    //Convert to float and return
    return [resultString floatValue];
}

/*
 * There must be a better way to do this
 * this is really really really really
 * *********** U G L Y *****************
 */

//Converts a float to a binary coded string
+ (NSString *)binaryStringWithFloat:(float)floatValue
{
    //Deternime sign bit
    char sign = '1';
    if(floatValue > 0) {
        sign = '0';
    }
    
    //If value negative, make positive
    if(floatValue < 0) {
        floatValue = -floatValue;
    }
    
    //Convert integral part
    NSString *integralString = [self binaryStringWithInteger:(uint8_t)floatValue];
    
    //Convert fraction part
    NSMutableString *fractionString = [[NSMutableString alloc] init];
    float fraction = floatValue - (int)floatValue;
    
    while(fraction > 0) {
        fraction = fraction * 2;
        if(fraction >= 1) {
            [fractionString appendString:@"1"];
            fraction = fraction - 1;
        } else {
            [fractionString appendString:@"0"];
        }
    }
    
    //Normalize
    NSString *normalizedString = [NSString stringWithFormat:@"%@.%@", integralString, fractionString];
    float unnormalizedFloatValue = [normalizedString floatValue];
    int exponent = 0;
    if((int)unnormalizedFloatValue > 0) {
        while(unnormalizedFloatValue > 1) {
            unnormalizedFloatValue = unnormalizedFloatValue / 10;
            exponent++;
        }
    } else if((int)unnormalizedFloatValue < 0) {
        while(unnormalizedFloatValue < 0) {
            unnormalizedFloatValue = unnormalizedFloatValue * 10;
            exponent--;
        }
    }
    normalizedString = [NSString stringWithFormat:@"%f", unnormalizedFloatValue];
    
    //Extract the mantissa from the normalized string. If needed shorten or append with 0's
    NSArray *subStrings = [normalizedString componentsSeparatedByString:@"."];
    NSString *mantissa = [subStrings objectAtIndex:1];
    
    if([mantissa length] > 4) {
        mantissa = [mantissa substringWithRange:NSMakeRange(0, 4)];
    } else if ([mantissa length] < 4) {
        NSMutableString *tempString = [NSMutableString stringWithString:mantissa];
        for(int i = 0; i < 4 - [mantissa length]; i++) {
            [tempString appendString:@"0"];
        }
        mantissa = [NSString stringWithString:tempString];
    }
    
    //Normalize (by book standard)
    int decimalMantissa = (int)[mantissa integerValue];
    if(decimalMantissa == 0) {
        decimalMantissa = 1000;
        exponent = -4;
    }
    while(decimalMantissa < 1000) {
        decimalMantissa = decimalMantissa * 10;
        exponent--;
    }
    if(exponent < -4) {
        exponent = -4;
    } else if(exponent > 3) {
        exponent = 3;
    }
    mantissa = [NSString stringWithFormat:@"%i", decimalMantissa];
    
    //Convert exponent into binary string
    NSString *exponentString = [self binaryStringWithInteger:4+exponent];
    exponentString = [exponentString substringFromIndex:5];
    
    //Assemble result
    NSString *result = [NSString stringWithFormat:@"%c%@%@", sign, exponentString, mantissa];
    
    return result;
}

@end

@implementation NSString (privateMethods)


@end
