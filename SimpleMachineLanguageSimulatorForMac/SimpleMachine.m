// Copyright (C) 2012 Backslashed

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "SimpleMachine.h"
#import "NSString+binaryString.h"

@interface SimpleMachine()

- (void)fetch; //fetch next instruction
- (void)execute; //execute instruction

- (void)incrementProgramCounter; //increment program counter
- (void)setValue:(uint8_t)value toRegister:(uint8_t)index; //set register value
- (void)setValue:(uint8_t)value toMemoryCell:(uint8_t)index; //set value in memory

/*
 * Simple Machine Language Instructions
 */
- (void)loadToRegister:(uint8_t)targetIndex fromMemory:(uint8_t)sourceIndex;
- (void)loadToRegister:(uint8_t)targetIndex withValue:(uint8_t)value;
- (void)storeFromRegister:(uint8_t)sourceIndex toMemory:(uint8_t)targetIndex;
- (void)moveFromRegister:(uint8_t)sourceIndex toRegister:(uint8_t)targetIndex;
- (void)addToRegister:(uint8_t)targetIndex withRegister:(uint8_t)firstSourceIndex andRegister:(uint8_t)secondSourceIndex;
- (void)addFloatToRegister:(uint8_t)targetIndex withRegister:(uint8_t)firstSourceIndex andRegister:(uint8_t)secondSourceIndex;
- (void)orToRegister:(uint8_t)targetIndex withRegister:(uint8_t)firstSourceIndex andRegister:(uint8_t)secondSourceIndex;
- (void)andToRegister:(uint8_t)targetIndex withRegister:(uint8_t)firstSourceIndex andRegister:(uint8_t)secondSourceIndex;
- (void)xorToRegister:(uint8_t)targetIndex withRegister:(uint8_t)firstSourceIndex andRegister:(uint8_t)secondSourceIndex;
- (void)rightRotateRegister:(uint8_t)targetIndex noOfSteps:(uint8_t)steps;
- (void)jumpToMemory:(uint8_t)targetIndex ifRegister0EqualsRegister:(uint8_t)sourceIndex;
- (void)halt;
- (void)leftShiftRegister:(uint8_t)targetIndex noOfSteps:(uint8_t)steps;
- (void)rightShiftRegister:(uint8_t)targetIndex noOfSteps:(uint8_t)steps;
- (void)unknownInstruction;

@end

@implementation SimpleMachine

- (id)init
{
    if(self = [super init]) {
        //Initialize memory and cpu
        [self resetCPU];
        [self resetMemory];
    }
    
    return self;
}

/*
 * Reset all registers to 0 and halt to NO
 */
- (void)resetCPU
{
    for(int i = 0; i < 16; i++) {
        _registers[i] = 0;
    }
    
    _programCounter = 0;
    
    _instructionRegister = 0;

    _halt = NO;
}

/*
 * Reset all memory cells
 */
- (void)resetMemory
{
    //Loop through all cells and set to 0
    for(int i = 0; i < 256; i++) {
        _mainMemory[i] = 0;
    }
}

//Run the machine
- (void)run
{
    _halt = NO;
    
    //Notify delegate that we are running
    [self.delegate didBeginRunning];
    
    //Decode, fetch and execute as long as _halt isn't set
    while(_halt == NO) {
        [self fetch];
        [self execute];
    }
    
    //Notify delegate that we have stopped running
    [self.delegate didStopRunning];
}

//Stop machine
- (void)stop
{
    _halt = YES;
}

//Single step machine
- (void)singleStep
{
    //Notify delegate that we are running
    [self.delegate didBeginRunning];
    
    [self fetch];
    [self execute];
    
    //Notify delegate that we have stopped running
    [self.delegate didStopRunning];
}

//Fetches the instruction that program counter is pointing to and stores it in the instruction register
- (void)fetch
{
    //Fetch instruction (2 memory cells) and place in instruction register
    _instructionRegister = 0;
    _instructionRegister = _mainMemory[_programCounter] << 8;
    _instructionRegister += _mainMemory[_programCounter+1];
    
    //Increment program counter
    [self incrementProgramCounter];
    
    //Notify delegate
    [self.delegate didUpdateInstructionRegisterWithValue:_instructionRegister];
}

//Executes instruction in instruction register
- (void)execute
{
    //Convert _instructionRegister[0] to a 2 character coded hex string, pick the first one to indicate which instruction
    char instruction = [[NSString hexStringWith16BitInteger:_instructionRegister] characterAtIndex:0];
    
    //Get op-codes so we can use them to point out memory and registry locations
    uint8_t opcode[2];
    opcode[0] = (uint8_t)(_instructionRegister >> 8)%16;
    opcode[1] = (uint8_t)_instructionRegister;

    switch (instruction) {
        case '1': //load adress
            [self loadToRegister:opcode[0] fromMemory:opcode[1]];
            break;
        case '2': //load
            [self loadToRegister:opcode[0] withValue:opcode[1]];
            break;
        case '3': //store
            [self storeFromRegister:opcode[0] toMemory:opcode[1]];
            break;
        case '4': //move
            [self moveFromRegister:opcode[0] toRegister:opcode[1]%16];
            break;
        case '5': //add
            [self addToRegister:opcode[0] withRegister:opcode[1]/16 andRegister:opcode[1]%16];
            break;
        case '6': //add float
            [self addFloatToRegister:opcode[0] withRegister:opcode[1]/16 andRegister:opcode[1]%16];
            break;
        case '7': //or
            [self orToRegister:opcode[0] withRegister:opcode[1]/16 andRegister:opcode[1]%16];
            break;
        case '8': //and
            [self andToRegister:opcode[0] withRegister:opcode[1]/16 andRegister:opcode[1]%16];
            break;
        case '9': //xpr
            [self xorToRegister:opcode[0] withRegister:opcode[1]/16 andRegister:opcode[1]%16];
            break;
        case 'A': //right rotate
            [self rightRotateRegister:opcode[0] noOfSteps:opcode[1]%16];
            break;
        case 'B': //jump
            [self jumpToMemory:opcode[1] ifRegister0EqualsRegister:opcode[0]];
            break;
        case 'C': //halt
            [self halt];
            break;
        case 'D': //left shift
            [self leftShiftRegister:opcode[0] noOfSteps:opcode[1]%16];
            break;
        case 'E': //right shift
            [self rightShiftRegister:opcode[0] noOfSteps:opcode[1]%16];
            break;
        default: //unknown instruction
            [self unknownInstruction];
            break;
    }
}

//Increments program counter, with loop around
- (void)incrementProgramCounter
{
    _programCounter+=2;
    
    if(_programCounter >= 255) {
        _programCounter = 0;
    }
    
    //Let the delegate know that the counter has been updated
    [self.delegate didUpdateProgramCounterWithValue:_programCounter];
}

//Sets a value to specified registry and notifies delegate
- (void)setValue:(uint8_t)value toRegister:(uint8_t)index
{
    _registers[index] = value;
    
    [self.delegate didUpdateRegisterAtIndex:index withValue:value];
}

//Set value to memory cell and notifies delegate
- (void)setValue:(uint8_t)value toMemoryCell:(uint8_t)index
{
    _mainMemory[index] = value;
    
    [self.delegate didUpdateMemoryAtIndex:index withValue:value];
}

/*
 * Getters and Setters
 */

- (uint8_t)getMemoryAtIndex:(uint8_t)index
{
    return _mainMemory[index];
}

- (void)setMemory:(uint8_t)value atIndex:(uint8_t)index
{
    _mainMemory[index] = value;
}

- (uint8_t)getRegisterAtIndex:(uint8_t)index
{
    return _registers[index];
}

- (void)setRegister:(uint8_t)value atIndex:(uint8_t)index
{
    _registers[index] = value;
}

- (void)setProgramCounter:(uint8_t)counter
{
    _programCounter = counter;
}

- (uint8_t)getProgramCounter
{
    return _programCounter;
}

- (void)setInstructionRegister:(uint16_t)instructionRegister
{
    _instructionRegister = instructionRegister;
}

- (uint16_t)getInstructionRegister
{
    return _instructionRegister;
}

/*
 * Simple Machine Language Instructions
 */
- (void)loadToRegister:(uint8_t)targetIndex fromMemory:(uint8_t)sourceIndex
{
    //Set register value with value in memory
    [self setValue:_mainMemory[sourceIndex] toRegister:targetIndex];
    
    //Send LOAD to delegate
    [self.delegate didRecieveMessage:@"LOAD"];
}

- (void)loadToRegister:(uint8_t)targetIndex withValue:(uint8_t)value
{
    //Save value to register
    [self setValue:value toRegister:targetIndex];
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"LOAD"];
}

- (void)storeFromRegister:(uint8_t)sourceIndex toMemory:(uint8_t)targetIndex
{
    //Save register value to memory
    [self setValue:_registers[sourceIndex] toMemoryCell:targetIndex];
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"STORE"];
}

- (void)moveFromRegister:(uint8_t)sourceIndex toRegister:(uint8_t)targetIndex
{
    //Copy value to target from source
    [self setValue:_registers[sourceIndex] toRegister:sourceIndex];
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"MOVE"];
}

- (void)addToRegister:(uint8_t)targetIndex withRegister:(uint8_t)firstSourceIndex andRegister:(uint8_t)secondSourceIndex
{
    //Add value in source registers and save in target register
    [self setValue:(_registers[firstSourceIndex])+(_registers[secondSourceIndex]) toRegister:targetIndex];
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"ADD"];
}

- (void)addFloatToRegister:(uint8_t)targetIndex withRegister:(uint8_t)firstSourceIndex andRegister:(uint8_t)secondSourceIndex
{
    //Fetch values from register
    uint8_t firstIntValue = _registers[firstSourceIndex];
    uint8_t secondIntValue = _registers[secondSourceIndex];
    
    //Convert to binary coded strings
    NSString *firstBinaryString = [NSString binaryStringWithInteger:firstIntValue];
    NSString *secondBinaryString = [NSString binaryStringWithInteger:secondIntValue];
    
    //Convert binary strings to float
    float firstFloatValue = [firstBinaryString parseBinaryStringToFloat];
    float secondFloatValue = [secondBinaryString parseBinaryStringToFloat];
    
    //Add floats
    float result = firstFloatValue + secondFloatValue;
    
    //Convert result to binary string
    NSString *resultBinaryString = [NSString binaryStringWithFloat:result];
    
    //Convert float binary string to int
    uint8_t integerResult = [resultBinaryString parseBinaryStringToInteger];
    
    //Save int
    [self setValue:integerResult toRegister:targetIndex];
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"ADD"];
}

- (void)orToRegister:(uint8_t)targetIndex withRegister:(uint8_t)firstSourceIndex andRegister:(uint8_t)secondSourceIndex
{
    //Get values from registers
    uint8_t firstValue = _registers[firstSourceIndex];
    uint8_t secondValue = _registers[secondSourceIndex];
    
    //bitwise OR on the two values
    uint8_t result = firstValue | secondValue;
    
    //Save result in target
    [self setValue:result toRegister:targetIndex];
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"OR"];
}

- (void)andToRegister:(uint8_t)targetIndex withRegister:(uint8_t)firstSourceIndex andRegister:(uint8_t)secondSourceIndex
{
    //Get values from registers
    uint8_t firstValue = _registers[firstSourceIndex];
    uint8_t secondValue = _registers[secondSourceIndex];
    
    //bitwise AND on the two values
    uint8_t result = firstValue & secondValue;
    
    //Save result in target
    [self setValue:result toRegister:targetIndex];
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"AND"];
}

- (void)xorToRegister:(uint8_t)targetIndex withRegister:(uint8_t)firstSourceIndex andRegister:(uint8_t)secondSourceIndex
{
    //Get values from registers
    uint8_t firstValue = _registers[firstSourceIndex];
    uint8_t secondValue = _registers[secondSourceIndex];
    
    //bitwise OR on the two values
    uint8_t result = firstValue ^ secondValue;
    
    //Save result in target
    [self setValue:result toRegister:targetIndex];
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"XOR"];
}

- (void)rightRotateRegister:(uint8_t)targetIndex noOfSteps:(uint8_t)steps
{
    //Get value from register
    uint8_t value = _registers[targetIndex];
    
    //Bitwise rotation
    value = (value >> steps) | (value << (8-steps));
    
    //Save result
    [self setValue:value toRegister:targetIndex];
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"ROTATE"];
}

- (void)jumpToMemory:(uint8_t)targetIndex ifRegister0EqualsRegister:(uint8_t)sourceIndex
{
    //If source index has the same value as register 0, change program counter to target index
    if(_registers[0] == _registers[sourceIndex]) {
        _programCounter = targetIndex;
    }
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"JUMP"];
}

- (void)halt
{
    //Set halt to YES
    _halt = YES;
    
    //Notify delegate which instruction we executed
    [self.delegate didRecieveMessage:@"HALT"];
}

- (void)leftShiftRegister:(uint8_t)targetIndex noOfSteps:(uint8_t)steps
{
    //Get value from register
    uint8_t result = _registers[targetIndex];
    
    //Left shift
    result = result << steps;
    
    //Save result
    [self setValue:result toRegister:targetIndex];
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"LSHIFT"];
}

- (void)rightShiftRegister:(uint8_t)targetIndex noOfSteps:(uint8_t)steps
{
    //Get value from register
    uint8_t result = _registers[targetIndex];
    
    //Right shift
    result = result >> steps;
    
    //Save result
    [self setValue:result toRegister:targetIndex];
    
    //Notify delegate
    [self.delegate didRecieveMessage:@"RSHIFT"];
}

- (void)unknownInstruction
{
    //Set halt to YES
    _halt = YES;
    
    //And notify delegate that we tried to execute an unknown instruction
    [self.delegate didRecieveMessage:@"Unknown instruction"];
}

@end