// Copyright (C) 2012 Backslashed

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import <Foundation/Foundation.h>

@protocol SimpleMachineDelegate

- (void)didUpdateMemoryAtIndex:(uint8_t)index withValue:(uint8_t)value; //Lets the delegate know which part of memory was updated with what
- (void)didUpdateRegisterAtIndex:(uint8_t)index withValue:(uint8_t)value; //Lets the delegate know which register was updated with what
- (void)didUpdateProgramCounterWithValue:(uint8_t)value; //Lets the delegate know which value the program counter was updated with
- (void)didUpdateInstructionRegisterWithValue:(uint16_t)value; //Lets the delegate know which value the instruction register was updated with

- (void)didBeginRunning; //Tell the delegate that the machine is running
- (void)didStopRunning; //Tell the delegate that the machine has stoppped running

- (void)didRecieveMessage:(NSString *)message; //Pass message to delegate

@end

@interface SimpleMachine : NSObject {
    uint8_t _mainMemory[256]; //Main memory.
    uint8_t _registers[16]; //Our 16 general purpose registers
    uint8_t _programCounter; //Program counter points out the next memory cell to read instruction from
    uint16_t _instructionRegister; //Instruction register is 16 bit with op-code and operands
    BOOL _halt; //halt flag
}

@property (weak, nonatomic) id <SimpleMachineDelegate> delegate; //Simple machine delegate, the reciever of messages from the machine

- (void)run; //Run the machine
- (void)stop; //Stop the machine
- (void)singleStep; //Run 1 instruction
- (void)resetMemory; //Resets the memory
- (void)resetCPU; //Resets all the registers


/*
 * Getters and Setters
 */
- (uint8_t)getMemoryAtIndex:(uint8_t)index;
- (void)setMemory:(uint8_t)value atIndex:(uint8_t)index;

- (uint8_t)getRegisterAtIndex:(uint8_t)index;
- (void)setRegister:(uint8_t)value atIndex:(uint8_t)index;

- (void)setProgramCounter:(uint8_t)counter;
- (uint8_t)getProgramCounter;

- (void)setInstructionRegister:(uint16_t)instructionRegister;
- (uint16_t)getInstructionRegister;

@end
